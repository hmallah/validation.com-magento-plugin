<?php
/**
 * Validation module registration
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Validationteam_Validation',
    __DIR__
);
