require(
    [
        'jquery'
    ],
    function ($) {
        /**
         * Adding URLs and creating event listener to communicate with Validation.com iframe page
         */
        var settingsUrl = $('[data-role="validation-iframe-container"]').attr('data-settings-url'),
            credentialsUrl = $('[data-role="validation-iframe-container"]').attr('data-credentials-url'),
            eventMethod = window.addEventListener ? "addEventListener" : "attachEvent",
            eventer = window[eventMethod],
            messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
        
        // Listen to message from child window
        eventer(messageEvent, eventCallback, false);

        /**
         * Event callback gathering two event messages 'addCloseButton' and 'closeThisModal'
         *     so we can tell Validation.com to add close button inside the page.
         * 
         * @param {Event} evt 
         */
        function eventCallback(evt)
        {
            var key = evt.message ? "message" : "data",
                data = evt[key];

            //EVENTS
            if (data.message === "alreadyHaveAccount") {
                window.location.href = settingsUrl;
            }
            else if (data.message === "getCredentials"
                && data.subId > 0
                && data.token.length > 0
            ) {
                $.ajax(
                    {
                        url: credentialsUrl,
                        type: "POST",
                        data: {
                            "sub_id": data.subId,
                            "token": data.token
                        },
                        dataType: "json",
                        error: function (data) {
                            console.log('ERROR: ', data);
                        },
                    }
                );
            }
        }
    }
);