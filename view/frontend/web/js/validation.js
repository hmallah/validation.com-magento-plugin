require(
    [
        'jquery'
    ],
    function ($) {
        $(
            function () {
                // to ensure that code evaluates on page load
                /**
                 * Creating event listener to communicate with Validation.com iframe page
                 */
                var modal = $("[data-role=validation-modal]").addClass("_show"),
                eventMethod = window.addEventListener ? "addEventListener" : "attachEvent",
                eventListener = window[eventMethod],
                messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";
            
                // Listen to message from child window
                eventListener(messageEvent, eventCallback, false);

                /**
                 * Event callback gathering two event messages 'addCloseButton' and 'closeThisModal'
                 *     so we can tell Validation.com to add close button inside the page.
                 * 
                 * @param {Event} evt 
                 */
                function eventCallback(evt)
                {
                    var key = evt.message ? "message" : "data",
                    data = evt[key];

                    //EVENTS
                    switch (data.message) {
                    case "addCloseButton":
                        var iframe = $('[data-role=validation-iframe]')[0];  
                        iframe.contentWindow.postMessage({message: "confirmCloseButton"}, "*");
                        break;
                    case "closeThisModal":
                        modal.removeClass("_show");
                    default:
                        break;
                    }
                }
            }
        );
    }
);