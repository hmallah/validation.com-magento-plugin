<?php
namespace Validationteam\Validation\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Data helper class for validation tokens
 */
class Data extends AbstractHelper
{
    const XML_PATH_VALIDATION_SETTINGS = 'validation_settings/';

    /**
     * Get configuration value
     *
     * @param string $field     Trigger event id
     * @param string $storeCode Store code
     *
     * @return \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public function getConfigValue($field, $storeCode = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeCode
        );
    }
    
    /**
     * Get Module Selected configuration value
     *
     * @param string $storeCode Store code
     *
     * @return \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public function getModulesSelectedConfig($storeCode = null)
    {
        return $this->getConfigValue(
            self::XML_PATH_VALIDATION_SETTINGS.'modules/modules_selected',
            $storeCode
        );
    }

    /**
     * Get Credentials configuration value
     *
     * @param string $fieldid   Trigger event id
     * @param string $storeCode Store code
     *
     * @return \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public function getCredentialsConfig($fieldid, $storeCode = null)
    {
        return $this->getConfigValue(
            self::XML_PATH_VALIDATION_SETTINGS.'credentials/'.$fieldid,
            $storeCode
        );
    }

    /**
     * Get Trigger Event configuration value
     *
     * @param string $fieldid   Trigger event id
     * @param string $storeCode Store code
     *
     * @return \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public function getTriggerEventConfig($fieldid, $storeCode = null)
    {
        return $this->getConfigValue(
            self::XML_PATH_VALIDATION_SETTINGS.'trigger_event/'.$fieldid,
            $storeCode
        );
    }

    /**
     * Get Trigger Event configuration value
     *
     * @param string $fieldid   Trigger event id
     * @param string $storeCode Store code
     *
     * @return \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public function getValidatedEmailsConfig($storeCode = null)
    {
        return $this->getConfigValue(
            self::XML_PATH_VALIDATION_SETTINGS.'validated_emails/validated',
            $storeCode
        );
    }

    /**
     * Get order by Id
     *
     * @param int $order_id Order Id
     *
     * @return array
     */
    public function getOrderByOrderId($order_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get(
            \Magento\Framework\App\ResourceConnection::class
        );
        $connection = $resource->getConnection();
        $select = $connection->select()
            ->from(
                ['vt' => 'validation_tokens'],
                ['token']
            )
            ->where('order_id = ?', $order_id);
        return $connection->fetchAll($select);
    }

    /**
     * Get path urls configuration
     *
     * @param string $fieldid   Path id
     * @param string $storeCode Store code
     *
     * @return array
     */
    public function getPathConfig($fieldid, $storeCode = null)
    {
        return $this->getConfigValue(
            self::XML_PATH_VALIDATION_SETTINGS.'path/'.$fieldid,
            $storeCode
        );
    }

    /**
     * Save client credentials
     *
     * @param string $order_id     Order Id
     *
     * @return array
     */
    public function setClientCredentials($order_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get(
            \Magento\Framework\App\ResourceConnection::class
        );
        $connection = $resource->getConnection();
        $select = $connection->select()
            ->from(
                ['vt' => 'validation_tokens'],
                ['token']
            )
            ->where('order_id = ?', $order_id);
        return $connection->fetchAll($select);
    }
}
