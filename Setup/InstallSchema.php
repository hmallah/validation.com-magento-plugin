<?php
namespace Validationteam\Validation\Setup;

/**
 * Validation tokens installation schema
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * Install schema
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup   Schema setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context Module context
     *
     * @return void
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('validation_tokens')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('validation_tokens')
            )
                ->addColumn(
                    'token_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'Token ID'
                )
                ->addColumn(
                    'order_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['nullable => false'],
                    'Order Id'
                )
                ->addColumn(
                    'token',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Token'
                )
                ->setComment('Validation Tokens Table');
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}
