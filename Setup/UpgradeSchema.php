<?php
namespace Validationteam\Validation\Setup;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Validation tokens upgrade schema
 */
class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    /**
     * Upgrade DB Schema
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup   Schema setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context Module context
     *
     * @return void
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $setup->endSetup();
    }
}
