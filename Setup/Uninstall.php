<?php
/**
 * @Uninstall
 * Uninstall validation.com plugin from your magento store.
 * @author    Validation Team <hicham@validation.com>
 * @license   GNU GPL 3.0 https://www.gnu.org/licenses/gpl-3.0.en.html
 * @link      https://validation.com
 */

namespace Validationteam\Validation\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Validation tokens uninstall
 *
 * @author   Validation Team <hicham@validation.com>
 * @license  GNU GPL 3.0 https://www.gnu.org/licenses/gpl-3.0.en.html
 * @link     https://validation.com
 */
class Uninstall implements UninstallInterface
{
    /**
     * Uninstall
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup   Schema setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context Module context
     *
     * @return void
     */
    public function uninstall(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->getConnection()->dropTable($setup->getTable('validation_tokens'));
    }
}
