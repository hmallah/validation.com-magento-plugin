<?php
namespace Validationteam\Validation\Block;

/**
 * Success block class, intended to check if validation is needed
 */
class Success extends \Magento\Framework\View\Element\Template
{
    /**
     * Checkout session
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * Validation data
     *
     * @var \Validationteam\Validation\Helper\Data
     */
    protected $_validationData;

    /**
     * Validation Token Factory
     *
     * @var \Validationteam\Validation\Model\ValidationTokenFactory
     */
    protected $_validationTokenFactory;

    /**
     * Result Page Factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Logger
     *
     * \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * Object Manager
     *
     * \Magento\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * Construct
     *
     * @param Session                $checkoutSession   Checkout session
     * @param Context                $context           Context
     * @param PageFactory            $resultPageFactory Page factory
     * @param Data                   $validationData    Validation data
     * @param ValidationTokenFactory $validationFactory Validation Factory
     * @param LoggerInterface        $logger            Logger
     * @param array                  $data              Data array
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Validationteam\Validation\Helper\Data $validationData,
        \Validationteam\Validation\Model\ValidationTokenFactory $validationFactory,
        \Psr\Log\LoggerInterface $logger,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_resultPageFactory = $resultPageFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_isScopePrivate = true;
        $this->_validationData = $validationData;
        $this->_validationTokenFactory = $validationFactory;
        $this->_logger = $logger;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        return $this->_resultPageFactory->create();
    }

    /**
     * Check if Validation is needed
     *
     * @return string
     */
    public function checkIfValidationIsNeeded()
    {
        $order = $this->_checkoutSession->getLastRealOrder();
        try {
            // check if the payment method is one we should pop up for
            // then checking if the order amout is above the requested
            $payment = $order->getPayment();
            $methods = $this->_validationData->getTriggerEventConfig(
                'payment_methods'
            );
            $categories = $this->_validationData->getTriggerEventConfig('product_categories');
            $payment_methods = [];
            $product_categories = [];
            if($methods != ''){
                $payment_methods = explode(',', $methods);
            }
            if($categories != ''){
                $product_categories = explode(',', $categories);
            }

            $validated_emails = $this->_validationData->getValidatedEmailsConfig();
            if($validated_emails !='') {
                $validated_emails_array = explode(",", $validated_emails);
                $validated_emails_array = array_map('trim', $validated_emails_array);
                $user_email = $order->getCustomerEmail();
                if (in_array($user_email, $validated_emails_array)) {
                    return "";
                }
            }
            $items = $order->getAllVisibleItems();



            foreach($items as $item){
                $product_id = $item->getProductId();
                $product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
                $category_ids = $product->getCategoryIds();
                if(!empty(array_intersect($category_ids, $product_categories))){
                    return $this->generateToken($order->getIncrementId());
                }
            }

            if (in_array($payment->getMethod(), $payment_methods) || empty($payment_methods)) {
                $orderTotal = doubleval($order->getGrandTotal());
                $on_order_above = $this->_validationData->getTriggerEventConfig(
                    'on_orders_above'
                );
                $valueToCompare = doubleval($on_order_above);
                if ($orderTotal >= $valueToCompare) {
                    return $this->generateToken($order->getIncrementId());
                }
            }
        } catch (\Exception $e) {
            $this->_logger->critical(
                'Error checking if validation is needed',
                ['exception' => $e]
            );
            // nothing to do here..
            return "";
        }
    }

    /**
     * Get submission URL
     *
     * @param string $token Token string
     *
     * @return string
     */
    public function getSubmitUrl($token)
    {
        return $this->_validationData->getPathConfig('submit_url') . $token;
    }

    /**
     * Generate token for order
     *
     * @param int $order_id Order Id
     *
     * @return string
     */
    protected function generateToken($order_id)
    {
        // Check if the token is inside the Database
        $validation_token = $this->_validationData->getOrderByOrderId($order_id);

        if (!empty($validation_token)) {
            return $validation_token[0]['token'];
        }

        // If not, generate it with our server
        $module_selected = $this->_validationData->getModulesSelectedConfig();
        $upload_types = explode(',', $module_selected);

        $data = [
            'TicketId' => 'Magento_OrderId-'.$order_id,
            'AllowUpload' => 0,
            'NotificationEmail' => '',
            'Remarks' => 'Token auto generated for Magento shop',
            'ShowDressedPop' => 1,
            'UploadTypesIds' => $upload_types,
            'Flagged' => 1,
            'SystemFlagged' => 1,
            'DaysToExpire' => 14,
            'TrackingId' => 'Magento_OrderId-'.$order_id
        ];

        $api_url = $this->_validationData->getPathConfig('api_url');

        $uri = $api_url.'tokens/generate';
        $config = [
            'adapter'   => \Zend\Http\Client\Adapter\Curl::class,
            'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
        ];
        $client = new \Zend\Http\Client($uri, $config);
        $client->setHeaders(
            [
                'clientId' => $this->_validationData->getCredentialsConfig(
                    'client_id'
                ),
                'clientSecret' => $this->_validationData->getCredentialsConfig(
                    'client_secret'
                ),
                'Content-Type' => 'application/json'
            ]
        );
        $json = json_encode($data);
        $client->setMethod('POST');
        $client->setRawBody($json);
        $result = $client->send();

        if ($result->isSuccess()) {
            $data = json_decode($result->getBody());
            $model = $this->_validationTokenFactory->create();
            $model->setData(
                [
                    "token" => $data->token,
                    "order_id" => $order_id
                ]
            )->save();
            return $data->token;
        }
        return "";
    }
}
