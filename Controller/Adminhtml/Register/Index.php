<?php
namespace Validationteam\Validation\Controller\Adminhtml\Register;

/**
 * Registration controller for Validation.com access credentials
 */
class Index extends \Magento\Backend\App\Action
{

    /**
     * Result Page Factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Validation data
     *
     * @var \Validationteam\Validation\Helper\Data
     */
    protected $_validationData;

    /**
     * Construct
     *
     * @param Context     $context           Context
     * @param Data        $validationData    Validation data
     * @param PageFactory $resultPageFactory Page factory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Validationteam\Validation\Helper\Data $validationData
    ) {
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
        $this->_validationData = $validationData;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Validationteam_Validation::configuration');
        $title = __('Validation.com registration page');
        $resultPage->getConfig()->getTitle()->prepend($title);
        $block = $resultPage->getLayout()->getBlock(
            'validationteam.validation.registration'
        );
        $block->setData(
            'registration_url',
            $this->_validationData->getPathConfig(
                'registration_url'
            )
        );
        return $resultPage;
    }

    /**
     * Checking if this module is authorized
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    protected function _isAllowed()
    {
        $module = 'Validationteam_Validation::configuration';
        return $this->_authorization->isAllowed($module);
    }
}
