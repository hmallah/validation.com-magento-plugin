<?php
namespace Validationteam\Validation\Controller\Adminhtml\Register;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Psr\Log\LoggerInterface;

/**
 * Validation credentials controller
 */
class Credentials extends Action
{
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $_configWriter;

    /**
     * Result value to return to front end
     *
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * Logger to debug exception message
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * Cache list to flush
     *
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $cacheTypeList;

    /**
     * Cache to clean backend
     *
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $cacheFrontendPool;

    /**
     * Validation data
     *
     * @var \Validationteam\Validation\Helper\Data
     */
    protected $_validationData;

    /**
     * Constructor
     *
     * @param WriterInterface   $configWriter      Writter interface to update client credentials
     * @param Context           $context           Context
     * @param JsonFactory       $resultJsonFactory Result json
     * @param LoggerInterface   $logger            Logger for exception message
     * @param Data              $validationData    Validation database access
     * @param TypeListInterface $cacheTypeList     Cache list to flush on credentials update
     * @param Pool              $cacheFrontendPool Cache backend to clean on credentials update
     */
    public function __construct(
        WriterInterface $configWriter,
        Context $context,
        JsonFactory $resultJsonFactory,
        LoggerInterface $logger,
        \Validationteam\Validation\Helper\Data $validationData,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool
    ) {
        parent::__construct($context);
        $this->_configWriter = $configWriter;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_logger = $logger;
        $this->_validationData = $validationData;
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * Execute post action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        
        $post = $this->getRequest()->getPostValue();
        $result = $this->_resultJsonFactory->create();

        if (!$post) {
            $response_data['message'] = 'Error';
            return $result->setData($response_data);
        }
        
        try {
            /**
             * Validating post values
             */
            $error = false;
            
            if (!\Zend_Validate::is(trim($post['sub_id']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['token']), 'NotEmpty')) {
                $error = true;
            }
            if ($error) {
                $response_data['message'] = 'Invalid post values';
                return $result->setData($response_data);
            }

            /**
             * Generate credentials
             */
            $credentials = $this->generateCredentials(
                $post['sub_id'],
                $post['token']
            );
            if (!empty($credentials)) {
                /**
                 * Saving credentials
                 */
                $this->_configWriter->save(
                    'validation_settings/credentials/client_id',
                    $credentials->clientId,
                    'default',
                    0
                );
                $this->_configWriter->save(
                    'validation_settings/credentials/client_secret',
                    $credentials->clientSecret,
                    'default',
                    0
                );
                /**
                 * Cleaning cache
                 */
                $this->flushCache();

                /**
                 * Returning OK Message
                 */
                $response_data['message'] = 'Done.';
                return $result->setData($response_data);
            }
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }

        $response_data['message'] = 'We can\'t process your request right now.';
        return $result->setData($response_data);
    }
 
    /**
     * Flushing cache to update data
     */
    protected function flushCache()
    {
        $_types = [
            'config',
            'layout',
            'block_html',
            'collections',
            'reflection',
            'db_ddl',
            'compiled_config',
            'eav',
            'customer_notification',
            'config_integration',
            'config_integration_api',
            'full_page',
            'config_webservice',
            'translate',
            'vertex'
        ];

        foreach ($_types as $type) {
            $this->cacheTypeList->cleanType($type);
        }
        foreach ($this->cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
 
    /**
     * Generating Validation.com client credentials
     *
     * @param string $subId Submission ID gathered from validation.com iframe
     * @param string $token Submission token gathered from validation.com iframe
     *
     * @return array Containing ClientId and ClientSecret values, null if there
     *                  was an error getting the values
     */
    protected function generateCredentials($subId, $token)
    {
        $partner_url = $this->_validationData->getPathConfig('partner_url');
        $uri = $partner_url.'app/subscriptions/partnercredentials';

        $config = [
            'adapter'   => \Zend\Http\Client\Adapter\Curl::class,
            'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
        ];
        $client = new \Zend\Http\Client($uri, $config);

        $hash = hash('md5', $subId . 'Gnv6bCBl2' . $token . 'FnQ5fBvB8');
        $client->setHeaders(
            [
                'subid' => $subId,
                'token' => $token,
                'hash' => $hash,
                'Content-Type' => 'application/json'
            ]
        );
        $client->setMethod('GET');
        $result = $client->send();

        if ($result->isSuccess()) {
            $data = json_decode($result->getBody());
            return $data;
        }
        return null;
    }
}
