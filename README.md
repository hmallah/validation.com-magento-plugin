# Validation.com
Validate your customers on high risk transactions to prevent chargebacks, or prevent account takeovers, by using Validation.com to request ID documents and selfies for you to review with your own eyes, not AI.

# How to install this extension?

Under your root folder, run the following command lines:

- composer require validationteam/validation
- php bin/magento setup:upgrade
- php bin/magento setup:di:compile
- php bin/magento cache:flush

# How to use Validation.com?

1. Register with us

On the Magento Admin Panel, navigate to Validation -> Register, this will open our registration page, go to plans and subscribe with us.

2. Set up your Validation settings

On the Magento Admin Panel, navigate to Validation -> Configuration and set your settings.

3. Verify your clients ID

Whenever a client sets an order, an ID verification page will trigger on checkout success.