<?php
namespace Validationteam\Validation\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Validation token resource model
 */
class ValidationToken extends AbstractDb
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('validation_tokens', 'token_id');
    }
}
