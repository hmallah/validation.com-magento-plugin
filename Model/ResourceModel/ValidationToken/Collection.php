<?php
namespace Validationteam\Validation\Model\ResourceModel\ValidationToken;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Collection class for validation tokens
 */
class Collection extends AbstractCollection
{
    /**
     * Id field name
     *
     * @var string
     */
    protected $_idFieldName = 'token_id';
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'Validationteam_Validation_token_collection';
    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'validation_token_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Validationteam\Validation\Model\ValidationToken::class,
            \Validationteam\Validation\Model\ResourceModel\ValidationToken::class
        );
    }
}
