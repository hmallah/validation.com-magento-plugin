<?php
namespace Validationteam\Validation\Model\Config\Source;

/**
 * Payment option array for multiselect on admin settings page
 */
class PaymentOptions implements \Magento\Framework\Option\ArrayInterface
{
    
    /**
     * Option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $list = [];
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();

        $paymentHelper = $objectManager->get(\Magento\Payment\Helper\Data::class);
        $allPaymentMethods = $paymentHelper->getPaymentMethods();

        foreach ($allPaymentMethods as $paymentCode => $payment) {
            if (array_key_exists('active', $payment)
                && $payment['active'] == 1
                && array_key_exists('title', $payment)
            ) {
                $list[] = ['value' => $paymentCode, 'label' => $payment['title']];
            }
        }
        return $list;
    }
}
