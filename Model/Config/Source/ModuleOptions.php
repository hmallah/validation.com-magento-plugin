<?php
namespace Validationteam\Validation\Model\Config\Source;

/**
 * Module option array for multiselect on admin settings page
 */
class ModuleOptions implements \Magento\Framework\Option\ArrayInterface
{
    
    /**
     * Option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '3', 'label' => 'Selfie'],
            ['value' => '1', 'label' => 'Gov. Photo ID (Full)'],
            ['value' => '6', 'label' => 'Credit Card (front)'],
            ['value' => '2', 'label' => 'Passport'],
            ['value' => '7', 'label' => 'Gov. Photo ID (front)'],
            ['value' => '11', 'label' => 'Driver\'s License (front)'],
            ['value' => '10', 'label' => 'Driver\'s License (full)'],
            ['value' => '5', 'label' => 'Utility Bill'],
            ['value' => '8', 'label' => 'Company Id'],
            ['value' => '9', 'label' => 'Business Card'],
            ['value' => '12', 'label' => '2nd Photo Id (full)'],
            ['value' => '13', 'label' => '2nd Photo Id (front)'],
            ['value' => '14', 'label' => 'Proof Of Payment']
        ];
    }
}
