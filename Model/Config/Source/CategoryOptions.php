<?php
namespace Validationteam\Validation\Model\Config\Source;

/**
 * Payment option array for multiselect on admin settings page
 */
class CategoryOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Option array
     *
     * @return array
     */

    public function toOptionArray()
    {
        $list = [];
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();

        $categoryCollection = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
        $categories = $categoryCollection->create();
        $categories->addAttributeToSelect('*');
        foreach ($categories as $category) {
            $list[] = ['value' => $category->getId(), 'label' => $category->getName()];
        }
        return $list;
    }


}
