<?php
namespace Validationteam\Validation\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Validationteam\Validation\Model\ResourceModel;

/**
 * Validation token model
 */
class ValidationToken extends AbstractModel implements IdentityInterface
{
    /**
     * Validation cache tag
     */
    const CACHE_TAG = 'validation_tokens';

    /**
     * Validation cache tag
     */
    protected $_cacheTag = self::CACHE_TAG;
    
    /**
     * Validation cache tag
     */
    protected $_eventPrefix = self::CACHE_TAG;
    
    /**
     * Validation define model
     *
     * @return void
     */
    protected function _construct()
    {
        $init = ResourceModel\ValidationToken::class;
        $this->_init($init);
    }
    
    /**
     * Get Identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    /**
     * Get default values
     *
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }
}
